<?php

require_once('lib/customize.php');
require_once('lib/helpers.php');
require_once('lib/enqueue-assets.php');
require_once('lib/sidebars.php');
require_once('lib/theme-support.php');
require_once('lib/navigation.php');
require_once('lib/include-plugins.php');
require_once('lib/comment-callback.php');
// require_once('lib/metaboxes.php');

// function _themename_button($atts = [], $content = null, $tag = '') {
//   extract(shortcode_atts([
//     'color' => 'red',
//     'text' => 'Button'
//   ], $atts, $tag));
//
//   return '<button style="background-color: ' . esc_attr($color) . '">' . do_shortcode($content) . '</button>';
// }
//
// add_shortcode('_themename_button', '_themename_button');

// function func($out, $pairs, $atts, $shortcode) {
//   return [
//     'color' => 'blue'
//   ];
// }
// add_filter( 'shortcode_atts__themename_button', 'func' );

// function _themename_icon($atts) {
//   extract(shortcode_atts([
//     'icon' => ''
//   ], $atts));
//
//   return '<i class="' . esc_attr($icon) . '" aria-hidden ></i>';
// }
//
// add_shortcode('_themename_icon', '_themename_icon');

function _themename_handle_delete_post() {
  if( isset($_GET['action']) && $_GET['action'] === '_themename_delete_post') {
    if(!isset($_GET['nonce']) || !wp_verify_nonce( $_GET['nonce'], '_themename_delete_post_' . $_GET['post'] )) {
      return;
    }
    $post_id = isset($_GET['post']) ? $_GET['post'] : null;
    $post = get_post((int)$post_id);
    if(empty($post)){
      return;
    }
    if(!current_user_can('delete_post', $post_id )) {
      return;
    }

    wp_trash_post($post_id);
    wp_safe_redirect(home_url());

    die;
  }
}

add_action('init', '_themename_handle_delete_post');

?>
